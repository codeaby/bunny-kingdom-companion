import {
  Collapse,
  IconButton,
  Table,
  TableCell,
  TableRow,
} from "@material-ui/core";
import { KeyboardArrowUp, KeyboardArrowDown } from "@material-ui/icons";
import React, { useState } from "react";

const RoundRow = ({ label, value, fiefs }) => {
  const [expanded, setExpanded] = useState(false);

  return (
    <>
      <TableRow>
        <TableCell padding="checkbox" size="small">
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => setExpanded(!expanded)}
          >
            {expanded ? <KeyboardArrowUp /> : <KeyboardArrowDown />}
          </IconButton>
        </TableCell>

        <TableCell component="th" scope="row">
          {label}
        </TableCell>
        <TableCell align="right">{value}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ padding: "0 0 0 64px" }} colSpan={3}>
          <Collapse in={expanded} timeout="auto" unmountOnExit>
            <Table size="small" aria-label="feudos">
              {Object.keys(fiefs).map((key) => (
                <TableRow key={key}>
                  <TableCell component="th" scope="row">
                    Feudo {key}
                  </TableCell>
                  <TableCell align="right">{fiefs[key].points}</TableCell>
                </TableRow>
              ))}
            </Table>
          </Collapse>
        </TableCell>
      </TableRow>
    </>
  );
};

export default RoundRow;
