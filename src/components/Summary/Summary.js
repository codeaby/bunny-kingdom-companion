import {
  Button,
  Paper,
  styled,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Typography,
} from "@material-ui/core";
import React from "react";
import { calculateRoundPoints } from "../../utils/BoardUtils";
import RoundRow from "./RoundRow/RoundRow";

const StyledTableContainer = styled(TableContainer)({
  marginTop: 20,
  padding: 16,
});

const Summary = ({ fiefs, rounds, endRound }) => {
  const totalActualRound = calculateRoundPoints(fiefs);
  const total = rounds.reduce((sum, round) => sum + round.points, 0);

  return (
    <StyledTableContainer component={Paper}>
      <Typography variant="h5" align="left">
        Puntajes
      </Typography>
      <Table style={{ margin: "16px 0" }} aria-label="simple table">
        <TableBody>
          <RoundRow
            label="Ronda Actual"
            value={totalActualRound}
            fiefs={fiefs}
          />

          {rounds.map((round) => (
            <RoundRow
              label={`Ronda ${round.round}`}
              value={round.points}
              fiefs={round.fiefs}
            />
          ))}
          <TableRow>
            <TableCell size="small" padding="checkbox"></TableCell>
            <TableCell component="th" scope="row">
              <b>Total</b>
            </TableCell>
            <TableCell align="right">
              <b>{total}</b>
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
      <Button fullWidth variant="contained" color="primary" onClick={endRound}>
        Finalizar ronda actual
      </Button>
    </StyledTableContainer>
  );
};

export default Summary;
