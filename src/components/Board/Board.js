import React, { useState } from "react";
import {
  calculateFiefPoints,
  calculateRoundPoints,
  initBoard,
  recalculateFiefs,
  replaceCell,
} from "../../utils/BoardUtils";
import { COLUMN_NAMES } from "../../utils/constants";
import Cell from "../Cell/Cell";
import Menu from "../Menu/Menu";
import Summary from "../Summary/Summary";
import { HeaderCell, Row, StyledContainer } from "./Board.styles";

const Board = ({ initialState }) => {
  const [board, setBoard] = useState(initBoard(initialState.map));
  const [fiefs, setFiefs] = useState({});
  const [selectedCell, setSelectedCell] = useState(null);
  const [rounds, setRounds] = useState([]);

  const handleSelectedCell = (event, cell) =>
    setSelectedCell({ target: event.currentTarget, cell });
  const handleCloseMenu = () => setSelectedCell(null);

  const editCell = (cell, update) => {
    const newBoard = recalculateFiefs(
      replaceCell(board, {
        ...cell,
        ...update,
      })
    );

    setBoard(newBoard);
    setFiefs(calculateFiefPoints(newBoard));

    handleCloseMenu();
  };

  const endRound = () => {
    setRounds([
      ...rounds,
      {
        round: rounds.length + 1,
        points: calculateRoundPoints(fiefs),
        fiefs,
      },
    ]);
  };

  return (
    <StyledContainer fixed>
      <Row>
        {COLUMN_NAMES.map((letter, index) => (
          <HeaderCell index={index} key={letter}>
            {letter}
          </HeaderCell>
        ))}
      </Row>
      {board.map((row, rowIndex) => (
        <Row key={`row-${rowIndex}`}>
          <HeaderCell>{rowIndex + 1}</HeaderCell>
          {row.map((cell, cellIndex) => (
            <Cell
              key={`${rowIndex}-${cellIndex}-${cell.terrain}`}
              onClick={(e) => handleSelectedCell(e, cell)}
              cell={cell}
            />
          ))}
        </Row>
      ))}
      <Summary fiefs={fiefs} rounds={rounds} endRound={endRound} />
      <Menu
        selectedCell={selectedCell}
        editCell={editCell}
        handleCloseMenu={handleCloseMenu}
      />
    </StyledContainer>
  );
};

export default Board;
