import { Box, Container, styled } from "@material-ui/core";

export const StyledContainer = styled(Container)({
  margin: "16px auto",
});

export const Row = styled(Box)({
  display: "flex",
  alignItems: "center",
});

export const HeaderCell = styled(Box)({
  margin: 3,
  flexGrow: ({ index }) => (index > 0 ? 1 : 0),
  textAlign: "center",
  minWidth: 18,
});
