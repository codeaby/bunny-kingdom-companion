import { Box, Paper, styled, Typography } from "@material-ui/core";
import { Pets, AccountBalance, BubbleChart } from "@material-ui/icons";
import React from "react";
import { TERRAIN_TYPES } from "../../utils/constants";
import Icon from "../Icon/Icon";

const LAVA = {
  BOTTOM: {
    height: 76,
    borderRight: "none",
    borderBottom: "6px solid rgb(199, 62, 62)",
    marginRight: 6,
    marginBottom: 0,
  },
  RIGHT: {
    height: 70,
    borderRight: "6px solid rgb(199, 62, 62)",
    borderBottom: "none",
    marginRight: 0,
    marginBottom: 6,
  },
  NONE: {
    height: 70,
    borderRight: "none",
    borderBottom: "none",
    marginRight: 6,
    marginBottom: 6,
  },
};

const StyledCell = styled(Paper)(({ lava, terrain }) => ({
  flex: 1,
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  flexDirection: "column",
  backgroundColor: TERRAIN_TYPES[terrain].backgroundColor,
  ...LAVA[lava],
}));

const CellIndicator = styled(Box)({
  display: "flex",
  height: 20,
  alignItems: "center",
  justifyContent: "center",
});

const Cell = ({ cell, onClick }) => {
  const { terrain, lava } = cell;

  return (
    <StyledCell onClick={onClick} terrain={terrain} lava={lava}>
      <CellIndicator>
        {cell.fief && (
          <Typography style={{ color: "black" }} variant="caption">
            F{cell.fief}
          </Typography>
        )}
      </CellIndicator>
      <CellIndicator>{cell.bunny && <Icon component={Pets} />}</CellIndicator>
      <CellIndicator style={{ marginTop: 2 }}>
        {cell.resource && <Icon component={BubbleChart} />}
        {cell.towers > 0 && <Icon component={AccountBalance} />}
      </CellIndicator>
    </StyledCell>
  );
};

export default Cell;
