import {
  ListItemIcon,
  styled,
  Typography,
  MenuItem as MaterialMenuItem,
} from "@material-ui/core";
import React from "react";
import Icon from "../Icon/Icon";

const StyledListItemIcon = styled(ListItemIcon)({
  minWidth: "auto",
  marginRight: 10,
});

const MenuItem = ({ onClick, icon, label }) => {
  return (
    <MaterialMenuItem onClick={onClick}>
      <StyledListItemIcon>
        <Icon component={icon} />
      </StyledListItemIcon>
      <Typography variant="inherit">{label}</Typography>
    </MaterialMenuItem>
  );
};

export default MenuItem;
