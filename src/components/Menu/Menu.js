import { Menu as MaterialMenu } from "@material-ui/core";
import { AccountBalance, BubbleChart, Cancel, Pets } from "@material-ui/icons";
import React from "react";
import { hasBuilding } from "../../utils/BoardUtils";
import {
  AVAILABLE_RESOURCE_BUILDINGS,
  TERRAIN_TYPES,
} from "../../utils/constants";
import MenuItem from "./MenuItem";

const Menu = ({ selectedCell, handleCloseMenu, editCell }) => {
  return (
    <MaterialMenu
      id="bunny-kingdom-board-menu"
      anchorEl={selectedCell ? selectedCell.target : null}
      keepMounted
      open={!!selectedCell}
      onClose={handleCloseMenu}
    >
      {selectedCell && (
        <div>
          <MenuItem
            icon={selectedCell.cell.bunny ? Cancel : Pets}
            label={selectedCell.cell.bunny ? "Quitar todo" : "Conejito"}
            onClick={() =>
              editCell(
                selectedCell.cell,
                selectedCell.cell.bunny
                  ? {
                      bunny: false,
                      towers: selectedCell.cell.terrain !== "CITY" ? 0 : 1,
                      resource: null,
                    }
                  : { bunny: true }
              )
            }
          />

          {selectedCell.cell.bunny && !hasBuilding(selectedCell.cell) && (
            <>
              <MenuItem
                icon={AccountBalance}
                label="1 Torre"
                onClick={() => {
                  editCell(selectedCell.cell, { towers: 1 });
                }}
              />

              <MenuItem
                icon={AccountBalance}
                label="2 Torres"
                onClick={() => {
                  editCell(selectedCell.cell, { towers: 2 });
                }}
              />

              {selectedCell.cell.terrain === "MOUNTAIN" && (
                <MenuItem
                  icon={AccountBalance}
                  label="3 Torres"
                  onClick={() => {
                    editCell(selectedCell.cell, { towers: 3 });
                  }}
                />
              )}

              {TERRAIN_TYPES[
                selectedCell.cell.terrain
              ].availableResourceBuildings.map((resource) => (
                <MenuItem
                  key={resource}
                  icon={BubbleChart}
                  label={resource}
                  onClick={() => {
                    editCell(selectedCell.cell, { resource });
                  }}
                />
              ))}

              {AVAILABLE_RESOURCE_BUILDINGS.map((resource) => (
                <MenuItem
                  key={resource}
                  icon={BubbleChart}
                  label={resource}
                  onClick={() => {
                    editCell(selectedCell.cell, { resource });
                  }}
                />
              ))}
            </>
          )}
        </div>
      )}
    </MaterialMenu>
  );
};

export default Menu;
