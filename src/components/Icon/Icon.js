import { styled, Icon as MaterialIcon } from "@material-ui/core";

const Icon = styled(MaterialIcon)({
  color: "black",
});

Icon.defaultProps = {
  fontSize: "small",
};

export default Icon;
