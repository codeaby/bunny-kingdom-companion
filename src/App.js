import Delete from "@material-ui/icons/Delete";
import {
  AppBar,
  IconButton,
  ThemeProvider,
  Toolbar,
  Typography,
  createMuiTheme,
  CssBaseline,
} from "@material-ui/core";

import bunnyKingdomMap from "./bunny-kingdom.json";
import Board from "./components/Board/Board";

function App() {
  const theme = createMuiTheme({
    palette: {
      type: "dark",
    },
  });

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" style={{ flexGrow: 1 }}>
            Bunny Kingdom Companion
          </Typography>
          <IconButton
            edge="end"
            color="inherit"
            aria-label="menu"
            onClick={() => {}}
          >
            <Delete />
          </IconButton>
        </Toolbar>
      </AppBar>
      <Board initialState={bunnyKingdomMap} />
    </ThemeProvider>
  );
}

export default App;
