export const COLUMN_NAMES = [
  "",
  "A",
  "B",
  "C",
  "D",
  "E",
  "F",
  "G",
  "H",
  "I",
  "J",
];

export const BOARD = {
  ROWS: 10,
  COLUMNS: 10,
  FIRST_COLUMN: 0,
  LAST_COLUMN: 9,
  FIRST_ROW: 0,
  LAST_ROW: 9,
};

export const TERRAIN_TYPES = {
  FOREST: {
    resource: "Madera",
    backgroundColor: "rgba(4, 130, 38, 0.5)",
    availableResourceBuildings: ["Hongos"],
  },
  CITY: {
    isCity: true,
    backgroundColor: "rgba(255,255,255,0.8)",
    availableResourceBuildings: [],
  },
  SEA: {
    resource: "Peces",
    backgroundColor: "rgba(17, 103, 242, 0.5)",
    availableResourceBuildings: ["Perlas"],
  },
  PLAINS: {
    backgroundColor: "rgba(237, 234, 157, 0.8)",
    availableResourceBuildings: [],
  },
  MOUNTAIN: {
    backgroundColor: "rgba(100, 100, 100, 0.5)",
    availableResourceBuildings: ["Oro", "Diamante", "Lingotes", "Piedra"],
  },
  PLANTATION: {
    resource: "Zanahorias",
    backgroundColor: "rgba(237, 145, 17, 0.8)",
    availableResourceBuildings: ["Especias"],
  },
};

export const AVAILABLE_RESOURCE_BUILDINGS = [
  "Madera",
  "Zanahorias",
  "Peces",
  "Mercado",
];
