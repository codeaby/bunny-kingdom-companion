import { BOARD, TERRAIN_TYPES } from "./constants";

export const hasBuilding = (cell) => cell.towers > 0 || cell.resource;

export const initBoard = (initialMap) =>
  initialMap.map((row, rowIndex) =>
    row.map((cell, colIndex) => ({
      ...cell,
      row: rowIndex,
      col: colIndex,
      towers: TERRAIN_TYPES[cell.terrain].isCity ? 1 : 0,
    }))
  );

export const replaceCell = (board, newCell) =>
  board.map((row) =>
    row.map((cell) =>
      cell.row === newCell.row && cell.col === newCell.col ? newCell : cell
    )
  );

export const recalculateFiefs = (board) => {
  const fiefs = [...Array(BOARD.ROWS).keys()].map(() =>
    new Array(BOARD.COLUMNS).fill(null)
  );

  let nextFief = 0;

  board.forEach((row, rowIndex) => {
    row.forEach((cell, cellIndex) => {
      if (cell.bunny && !fiefs[rowIndex][cellIndex]) {
        fiefs[rowIndex][cellIndex] = ++nextFief;

        checkAdjacentCells(board, fiefs, rowIndex, cellIndex, nextFief);
      }
    });
  });

  return board.map((row, rowIndex) =>
    row.map((cell, cellIndex) => ({
      ...cell,
      fief: fiefs[rowIndex][cellIndex],
    }))
  );
};

/**
 * Check if bunny on next cell and add to same fief
 */
const checkNextCell = (board, fiefs, rowIndex, cellIndex, nextFief) => {
  if (
    cellIndex >= BOARD.FIRST_COLUMN &&
    cellIndex <= BOARD.LAST_COLUMN &&
    rowIndex >= BOARD.FIRST_ROW &&
    rowIndex <= BOARD.LAST_ROW &&
    !fiefs[rowIndex][cellIndex] &&
    board[rowIndex][cellIndex].bunny
  ) {
    fiefs[rowIndex][cellIndex] = nextFief;

    checkAdjacentCells(board, fiefs, rowIndex, cellIndex, nextFief);
  }
};

/**
 * Check right, bottom, left and top cells for more bunnies in the same fief
 */
const checkAdjacentCells = (board, fiefs, rowIndex, cellIndex, nextFief) => {
  if (board[rowIndex][cellIndex].lava !== "RIGHT")
    checkNextCell(board, fiefs, rowIndex, cellIndex + 1, nextFief);
  if (board[rowIndex][cellIndex].lava !== "BOTTOM")
    checkNextCell(board, fiefs, rowIndex + 1, cellIndex, nextFief);
  if (
    cellIndex > BOARD.FIRST_COLUMN &&
    board[rowIndex][cellIndex - 1].lava !== "RIGHT"
  )
    checkNextCell(board, fiefs, rowIndex, cellIndex - 1, nextFief);
  if (
    rowIndex > BOARD.FIRST_ROW &&
    board[rowIndex - 1][cellIndex].lava !== "BOTTOM"
  )
    checkNextCell(board, fiefs, rowIndex - 1, cellIndex, nextFief);
};

const getAmountOfDifferentResources = (resources) => {
  const filteredResources = resources.filter(
    (resource) => resource !== "Mercado"
  );
  const basicResources = resources.filter(
    (resource) =>
      resource === "Madera" || resource === "Peces" || resource === "Zanahorias"
  );

  return (
    filteredResources.length +
    Math.min(
      resources.length - filteredResources.length,
      3 - basicResources.length
    )
  );
};

export const calculateFiefPoints = (board) => {
  const fiefs = {};

  board.forEach((row) => {
    row.forEach((cell) => {
      if (cell.fief) {
        if (!fiefs[cell.fief]) {
          fiefs[cell.fief] = {
            towers: 0,
            resources: [],
          };
        }

        if (
          TERRAIN_TYPES[cell.terrain].resource &&
          !fiefs[cell.fief].resources.includes(
            TERRAIN_TYPES[cell.terrain].resource
          )
        ) {
          fiefs[cell.fief].resources.push(TERRAIN_TYPES[cell.terrain].resource);
        }

        if (cell.towers) {
          fiefs[cell.fief].towers += cell.towers;
        } else if (
          cell.resource &&
          (!fiefs[cell.fief].resources.includes(cell.resource) ||
            cell.resource === "Mercado")
        ) {
          fiefs[cell.fief].resources.push(cell.resource);
        }
      }
    });
  });

  Object.keys(fiefs).forEach((key) => {
    fiefs[key].points =
      fiefs[key].towers * getAmountOfDifferentResources(fiefs[key].resources);
  });

  return fiefs;
};

export const calculateRoundPoints = (fiefs) =>
  Object.keys(fiefs).reduce((sum, key) => sum + fiefs[key].points, 0);
